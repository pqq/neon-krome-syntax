# Nifty Notes

How to create a theme:  
https://flight-manual.atom.io/hacking-atom/sections/creating-a-theme/  

Location of Atom packages:  
`/home/user/.atom/packages/`  

Symbolic link creation (from > to):  
`/home/user/.atom/packages/neon-krome-syntax/` > `/run/media/user/gitRepos/gitlab/neon-krome-syntax/`  

How to publish a theme (need to use github.com):  
https://flight-manual.atom.io/hacking-atom/sections/publishing/
