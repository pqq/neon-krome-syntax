# neon-krome-syntax

NeonKrome - Syntax theme for Atom.io

Neon colours, optimised for:
- Color vision deficiency ("colour blindness")
- BLUblox Sleep+ glasses

![A screenshot of your theme](assets/screenshot_v001_atomMaterial.png)

Tested with:  
- Atom Material UI theme.

